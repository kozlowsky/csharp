﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    public class UrlData
    {
        public static readonly string WebsiteApiUrl = "https://cla.heroes-online.com/pl-PL//api/user/login";
        public static readonly string WebsiteUrl = "https://cla.heroes-online.com";

        public static readonly string AuthLoginUrl = "http://login01.gw01.cla.heroes-online.com/auth";
        public static readonly string SyncLoginUrl = "http://login01.gw01.cla.heroes-online.com/sync";
        //public static readonly string pingLoginUrl = "http://login01.gw01.cla.heroes-online.com/ping";

        public static readonly string AuthServerUrl = "http://gs01.gw01.cla.heroes-online.com/auth";
        public static readonly string SyncServerUrl = "http://gs01.gw01.cla.heroes-online.com/sync";
        //public static readonly string pingServerUrl = "http://gs01.gw01.cla.heroes-online.com/ping";
    }
}
