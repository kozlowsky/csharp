﻿using System;
using System.ComponentModel;

namespace HOMMOClientTest
{
    class ConsoleData : INotifyPropertyChanged
    {
        private static ConsoleData instance;
        private String data;
        public int DebugLevel { get; set; }
        private ConsoleData() { }

        public static ConsoleData Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConsoleData();
                }
                return instance;
            }
        }

        public String Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                OnPropertyChanged("Data");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
    }
}