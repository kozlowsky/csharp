﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace HOMMOClientTest
{
    public partial class MainWindow : Window
    {
        private List<byte> bufferData;
        public static List<Hero> heroes;

        private int sessionToken = 0;
        private string sessionId = "45WhBqPwTqz5K0IPhxLJ2hKXpvcEWyLT";

        public static bool serverDataReceived = false;
        public static bool abortExecution = false;

        public static int shopItemsLastLoad = 0;

        private List<int> objectsNetSyncList;

        private Networking networking;

        public MainWindow()
        {
            InitializeComponent();

            consoleScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            bufferData = new List<byte>();
            heroes = new List<Hero>();
            objectsNetSyncList = new List<int>();

            sessionIdInput.Text = sessionId;

            Binding binding = new Binding("Data");
            binding.Source = ConsoleData.Instance;
            outputConsole.SetBinding(TextBlock.TextProperty, binding);

            networking = new Networking();
        }

        private void sendMsgButton_Click(object sender, RoutedEventArgs e)
        {
            string username = msgIdInput.Text;

            if (username.Length < 3)
            {
                Debug.Log(DebugType.ERROR, "[ERR] Username too short!");
                return;
            }

            AddMessageToQueue(1004, (byte)4, username); // ACCOUNT_COMMAND COMMAND=PLAY

            Synchronize();
        }

        private IMessage getMessageObject(ushort messageCode)
        {
            var ns = typeof(MessageType).Namespace;
            var typeName = ns + "." + (MessageType)messageCode + "Message";
            return (IMessage)Activator.CreateInstance(Type.GetType(typeName));
        }

        private void ProcessResponse(Stream networkStream)
        {
            MemoryStream stream = new MemoryStream();
            networkStream.CopyTo(stream);
            networkStream.Close();

            stream.Position = 0;

            if (stream.Length == 0)
            {
                Debug.Log(DebugType.INFO, "[SYS] Received stream is empty...");
                return;
            }

            if (stream.Length == 8)
            {
                sessionToken = stream.ReadInt();
                sessionTokenLabel.Text = String.Format("Session Token: {0}", sessionToken);

                Debug.Log(DebugType.INFO, "[INFO] Session Token: " + sessionToken);
                Debug.Log(DebugType.INFO, "[INFO] Encrypt Enabled: " + stream.ReadInt());
            }

            while (stream.Position < stream.Length)
            {
                ushort currentMessageCode = stream.ReadUnsignedShort();
                ushort currentMessageSize = stream.ReadUnsignedShort();

                long currentStreamPosition = stream.Position;

                Debug.Log(DebugType.INFO, String.Format("[SYS] Received Message: [{0}] {1}", currentMessageCode, (MessageType)currentMessageCode));

                IMessage messageObject = getMessageObject(currentMessageCode);

                try
                {
                    messageObject.processStream(stream);
                }
                catch (MessageNotSupportedException e)
                {
                    Debug.Log(DebugType.WARNING, "[SYS] Skipping message...");
                    stream.Position = currentStreamPosition + currentMessageSize;
                }
                catch (InvalidSessionTokenException e)
                {
                    networking.LoginUsingHttp(ref sessionId);
                    sessionIdInput.Text = sessionId;
                }
            }

            stream.Close();
        }

        private void JoinServer()
        {
            Debug.Log(DebugType.INFO, "[CLIENT] Syncing with game server...");

            bufferData.Clear();

            if (!serverDataReceived)
            {
                AddMessageToQueue(20);
                Synchronize();
            }

            AddMessageToQueue(1440); // ARENA_REQUEST_REWARD
            AddMessageToQueue(1003); // SETTINGS_REQUEST
            AddMessageToQueue(1200); // CURRENCY_SHOP_URL_REQUEST
            AddMessageToQueue(9); // SUPPORT_PRIVILEGES
            //AddMessageToQueue(330, 5, 2193); // MONITOR_COMMAND(MONITOR_LOAD_INIT=5, 20193)
            ////AddMessageToQueue(330, 6, 1716); // MONITOR_COMMAND(MONITOR_LOAD_ACCOUNT=6, 1716)
            ////AddMessageToQueue(13, 0, 23300); // LOADING_TIME
            AddMessageToQueue(15, "WIN 26,0,0,131", (byte)1, 1024, 768); // CLIENT_INFO
            AddMessageToQueue(824); // SHOP_REQUEST_GAMEPLAY
            AddMessageToQueue(825, shopItemsLastLoad); // SHOP_REQUEST_UPDATE_STATE
            AddMessageToQueue(824); // SHOP_REQUEST_GAMEPLAY
            AddMessageToQueue(824); // SHOP_REQUEST_GAMEPLAY

            Stream receiveStream = networking.SendStream(UrlData.SyncServerUrl, sessionToken, sessionId, bufferData);

            if (receiveStream != null)
                ProcessResponse(receiveStream);
            Debug.Log(DebugType.INFO, "Connected!");
        }

        private void ServerAuth()
        {
            Debug.Log(DebugType.INFO, "[CLIENT] Authenticating game server...");

            bufferData.Clear();
            AddMessageToQueue(12, 2);
            AddMessageToQueue(824);

            Stream receiveStream = networking.SendStream(UrlData.SyncServerUrl, sessionToken, sessionId, bufferData);

            if (receiveStream != null)
            {
                Debug.Log(DebugType.INFO, "[INFO] Session Token: " + receiveStream.ReadInt());
                Debug.Log(DebugType.INFO, "[INFO] Encrypt Enabled: " + receiveStream.ReadInt());

                receiveStream.Close();

                if (!abortExecution)
                    JoinServer();
            }
        }

        private void GetServerInfo()
        {
            Debug.Log(DebugType.INFO, "[CLIENT] Getting server info...");

            bufferData.Clear();

            Stream receiveStream = networking.SendStream(UrlData.SyncLoginUrl, sessionToken, sessionId, bufferData);

            if (receiveStream != null)
            {
                ProcessResponse(receiveStream);

                if (!abortExecution)
                    ServerAuth();
            }
        }

        private void GetLoginToken()
        {
            Debug.Log(DebugType.INFO, "[CLIENT] Getting login token...");

            bufferData.Clear();

            bufferData.WriteInt(0);
            bufferData.WriteString(sessionId);

            Stream receiveStream = networking.SendStream(UrlData.AuthLoginUrl, sessionToken, sessionId, bufferData);

            if (receiveStream != null)
            {
                ProcessResponse(receiveStream);

                if (!abortExecution)
                    GetServerInfo();
            }
        }

        private void AddMessageToQueue(short messageId, params object[] args)
        {
            Debug.Log(DebugType.INFO, "[CLIENT] Adding Message to queue: ({0})", messageId);

            bufferData.WriteShort(messageId);

            int lengthPlaceholder = bufferData.Count;
            bufferData.WriteShort(0);

            int initPosition = bufferData.Count;

            foreach (object obj in args)
            {
                if (obj is byte)
                    bufferData.WriteByte((byte)obj);
                else if (obj is int)
                    bufferData.WriteInt((int)obj);
                else if (obj is short)
                    bufferData.WriteShort((short)obj);
                else if (obj is string)
                    bufferData.WriteUnicode((string)obj);
            }

            short messageLength = (short)(bufferData.Count - initPosition);
            bufferData.WriteShort(messageLength, lengthPlaceholder);
        }

        private void Synchronize()
        {
            Debug.Log(DebugType.INFO, "[CLIENT] Sending Buffer...");

            Stream receiveStream = networking.SendStream(UrlData.SyncServerUrl, sessionToken, sessionId, bufferData);

            if (receiveStream != null)
            {
                ProcessResponse(receiveStream);
            }
            else
            {
                Debug.Log(DebugType.VERBOSE, "Received response is empty...");
            }

            bufferData.Clear();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            abortExecution = false;

            Debug.Log(DebugType.INFO, "Logging with session id to CLA01...");

            GetLoginToken();
            //LoginFromWebsite();
        }

        private void syncButton_Click(object sender, RoutedEventArgs e)
        {
            Synchronize();
        }

        private void debugLevelDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ConsoleData.Instance.DebugLevel = comboBox.SelectedIndex;
        }

        private void clearConsoleButton_Click(object sender, RoutedEventArgs e)
        {
            ConsoleData.Instance.Data = String.Empty;
        }

        private void outputConsole_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            consoleScrollViewer.ScrollToBottom();
        }
    }
}
