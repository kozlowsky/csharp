﻿namespace HOMMOClientTest
{
    public enum DebugType
    {
        VERBOSE,
        INFO,
        WARNING,
        ERROR
    }
}
