﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class InvalidSessionTokenException : Exception
    {
        public InvalidSessionTokenException()
        {

        }

        public InvalidSessionTokenException(string message) : base(message)
        {

        }
    }
}
