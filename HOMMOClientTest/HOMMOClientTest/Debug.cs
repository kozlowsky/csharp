﻿using System;

namespace HOMMOClientTest
{
    class Debug
    {
        public static void Log(DebugType debugType, string input, params object[] args)
        {
            switch (debugType)
            {
                case DebugType.VERBOSE:
                    if (ConsoleData.Instance.DebugLevel == 0)
                        ConsoleData.Instance.Data += String.Format(input, args) + "\n";
                    break;
                case DebugType.INFO:
                    if (ConsoleData.Instance.DebugLevel < 2)
                        ConsoleData.Instance.Data += "[INFO] " + String.Format(input, args) + "\n";
                    break;
                case DebugType.WARNING:
                    if (ConsoleData.Instance.DebugLevel < 3)
                        ConsoleData.Instance.Data += "[WARN] " + String.Format(input, args) + "\n";
                    break;
                case DebugType.ERROR:
                    MainWindow.abortExecution = true;
                    ConsoleData.Instance.Data += "[ERR] " + String.Format(input, args) + "\n";
                    break;
            }

            //if (debugType > DebugType.WARNING)
                //MessageBox.Show(String.Format(input, args), "Error");
        }
    }
}