﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    public class Hero
    {
        public int GUID { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int Faction { get; set; }
        public int ClassName { get; set; }
        public int MapGUID { get; set; }
        public int ActionPoints { get; set; }
        public int PortraitGUID { get; set; }
        public int ExtendedLevel { get; set; }
        public string GuildName { get; set; }

        public Hero()
        {
            GUID = 0;
            Name = "";
            Level = 1;
            Faction = 0;
            ClassName = 0;
            MapGUID = 0;
            ActionPoints = 0;
            PortraitGUID = 0;
            ExtendedLevel = 0;
            GuildName = "";
        }

        public Hero(int guid, string name, int level, int faction, int className, int mapGUID, int actionPoints, int portraitGUID, int extendedLevel, string guildName)
        {
            this.GUID = guid;
            this.Name = name;
            this.Level = level;
            this.Faction = faction;
            this.ClassName = className;
            this.MapGUID = mapGUID;
            this.ActionPoints = actionPoints;
            this.PortraitGUID = portraitGUID;
            this.ExtendedLevel = extendedLevel;
            this.GuildName = guildName;
        }
    }
}
