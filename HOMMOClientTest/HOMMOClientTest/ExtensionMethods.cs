﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    public static class ExtensionMethods
    {
        public static uint ReadUnsignedInt(this Stream stream)
        {
            var intData = new byte[4];
            stream.Read(intData, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(intData);

            return BitConverter.ToUInt32(intData, 0);
        }

        public static int ReadInt(this Stream stream)
        {
            var intData = new byte[4];
            stream.Read(intData, 0, 4);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(intData);

            return BitConverter.ToInt32(intData, 0);
        }

        public static ushort ReadUnsignedShort(this Stream stream)
        {
            var ushortData = new byte[2];
            int result = stream.Read(ushortData, 0, 2);

            if (result >= 0)
            {
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(ushortData);

                return BitConverter.ToUInt16(ushortData, 0);
            }
            else
            {
                return 0;
            }
        }

        public static short ReadShort(this Stream stream)
        {
            var shortData = new byte[2];
            stream.Read(shortData, 0, 2);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(shortData);

            return BitConverter.ToInt16(shortData, 0);
        }

        public static string ReadString(this Stream stream)
        {
            int length = stream.ReadUnsignedShort();
            var stringData = new char[length];

            for(int i = 0; i < length; i++)
            {
                stringData[i] = Convert.ToChar(stream.ReadUnsignedShort());
            }

            return new string(stringData);
        }

        public static void WriteByte(this List<byte> buffer, byte bytes)
        {
            buffer.Add(bytes);
        }


        public static void WriteInt(this List<byte> buffer, int input, bool reverse = true)
        {
            var intData = BitConverter.GetBytes(input);

            if (BitConverter.IsLittleEndian && reverse)
                Array.Reverse(intData);

            buffer.AddRange(intData);
        }

        public static void WriteShort(this List<byte> buffer, short input, int position = 0)
        {
            var shortData = BitConverter.GetBytes(input);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(shortData);

            if (position > 0)
            {
                buffer[position] = shortData[0];
                buffer[position + 1] = shortData[1];
            }
            else
            {
                buffer.AddRange(shortData);
            }
        }

        public static void WriteString(this List<byte> buffer, string input)
        {
            var stringBytes = Encoding.UTF8.GetBytes(input);
            var length = BitConverter.GetBytes((short)stringBytes.Length);

            Array.Reverse(length);

            buffer.AddRange(length);
            buffer.AddRange(stringBytes);
        }

        public static void WriteUnicode(this List<byte> buffer, string input)
        {
            short length = (short) input.Length;

            buffer.WriteShort(length);

            int position = 0;

            while(position < length)
            {
                buffer.WriteShort((short)input[position]);

                position++;
            }
        }
    }
}
