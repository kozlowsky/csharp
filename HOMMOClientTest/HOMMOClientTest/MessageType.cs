﻿namespace HOMMOClientTest
{
    enum MessageType
    {
        Error,
        Redirect = 2,
        RedirectToLogin,
        ServerData = 5,
        JoinMap,
        SupportPrivileges = 9,
        Killswitch = 21,
        SetPlayerHero = 99,
        PlayerResources,
        HeroCurrency,
        HeroAchievementPoints,
        HeroXp,
        PlayerMasteries = 106,
        HeroMana,
        HeroActionPoints = 109,
        HeroDragonXp = 132,
        ObjectCreate = 151,
        ObjectDelete,
        ObjectUpdate,
        BattleCommands = 501,
        ShopGameplayItems = 822,
        ShopUpdateState = 826,
        SocialCommand = 1000,
        SettingsCommand = 1002,
        AccountCommand = 1004,
        HeroDataInvalid = 1113,
        CirrencyShopUrlUpdate = 1201,
        AchievementSync = 1400,
        AchievementUpdate,
        UpdateActiveGameEvents = 1450,
        HappyhourList = 1490,
        InviteChat = 1700,
        RiftSeasonInformation = 1736
    }
}
