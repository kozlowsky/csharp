﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class ServerDataMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            MainWindow.serverDataReceived = true;

            int serverVersion1 = stream.ReadInt();
            int killSwitch = stream.ReadInt();
            string serverVersion2 = stream.ReadString();
            string serverVersion3 = stream.ReadString();
            string errorReportingUrl = stream.ReadString();
            int heroDeletionCooldown = stream.ReadInt();
            int lastHeroDeletion = stream.ReadInt();
            int accountID = stream.ReadInt();
            string ABTest = stream.ReadString();

            //accountIdLabel.Text = String.Format("Account ID: {0}", accountID.ToString());
            Debug.Log(DebugType.INFO, String.Format("Account ID: {0}", accountID.ToString()));

            /*Log("Server Info: {0}, {1}, {2}", serverVersion1, serverVersion2, serverVersion3);
            Log("KillSwitch: {0}", killSwitch);
            Log("Error Reporting URL: {0}", errorReportingUrl);
            Log("Hero Deletion CoolDown: {0}", heroDeletionCooldown);
            Log("Last Hero Deletion: {0}", lastHeroDeletion);
            Log("Account ID: {0}", accountID);
            Log("ABTest: {0}", ABTest);*/
        }
    }
}
