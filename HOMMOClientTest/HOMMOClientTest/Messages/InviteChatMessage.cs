﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class InviteChatMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            string chatChannel = stream.ReadString();
            string param2 = stream.ReadString();

            Debug.Log(DebugType.VERBOSE, "Chat Details: {0}, {1}", chatChannel, param2);
        }
    }
}
