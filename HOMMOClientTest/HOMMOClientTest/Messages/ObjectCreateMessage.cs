﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class ObjectCreateMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            int netSyncID = stream.ReadInt();
            int assetGUID = stream.ReadInt();
            int assetType = stream.ReadByte();
            int assetVariation = stream.ReadByte();

            /*if (netSyncID >= 1000000000)
                objectsNetSyncList.Add(netSyncID - 1000000000);
            else
                objectsNetSyncList.Add(netSyncID);*/

            if (assetGUID <= -1)
            {
                //create object (assetType)
                switch (assetType)
                {
                    case 10: //AssetType.ARMY
                             //return new ArmyObject()
                        int team = stream.ReadInt();

                        break;
                    case 46: //AssetType.HERO_ACTIVATION
                             //return new HeroActivationObject()
                        int objectNetSyncID = stream.ReadInt();
                        //objectsNetSyncList.Add(objectNetSyncID);

                        break;
                    default:
                        break;
                }
            }
            else
            {
                //create object by guid (assetGUID, assetType, assetVariation)

                switch (assetType)
                {
                    case 3: // HERO
                        int armyNetSyncID = stream.ReadInt();
                        //objectsNetSyncList.Add(armyNetSyncID);

                        int heroID = stream.ReadInt();
                        string heroName = stream.ReadString();
                        int portraitGUID = stream.ReadInt();
                        int heroLevel = stream.ReadInt();
                        int extendedLevel = stream.ReadInt();
                        int battleRetryCount = stream.ReadByte();
                        int raidNumber = stream.ReadInt();
                        int heroicTier = stream.ReadInt();

                        break;
                    case 28: // ARTEFACT
                        int artefactOwnerNetSyncID = stream.ReadInt();
                        //objectsNetSyncList.Add(artefactOwnerNetSyncID);

                        break;
                    case 37: // GENERATED_ARTEFACT
                        int generatedArtefactOwnerNetSyncID = stream.ReadInt();
                        //objectsNetSyncList.Add(generatedArtefactOwnerNetSyncID);

                        short artefactLevel = stream.ReadShort();
                        int artefactAssetID = stream.ReadInt();
                        short artefactSuffixType = stream.ReadShort();
                        short artefactNameIndex = stream.ReadShort();
                        short artefactRarity = stream.ReadShort();

                        for (int i = 1; i < 7; i++)
                        {
                            short val2 = stream.ReadShort();
                        }

                        string artefactUpgradeID = stream.ReadString();

                        int secondaryStatsCount = stream.ReadInt();

                        for (int i = 0; i < secondaryStatsCount; i++)
                        {
                            int artefactSecondaryStatTarget = stream.ReadInt();
                            short artefactSecondaryStatType = stream.ReadShort();
                            short artefactSecondaryStatValue = stream.ReadShort();
                        }

                        break;
                    case 42: // EQUIPMENT_INVENTORY
                        int equipmentInventoryHero = stream.ReadInt();
                        int equipmentInventoryID = stream.ReadInt();

                        break;
                    case 50: // SLOT_CONSUMABLE_INVENTORY
                        int slotConsumableHero = stream.ReadInt();
                        int slotConsumableID = stream.ReadInt();

                        break;
                    default:
                        Debug.Log(DebugType.WARNING, "[WARN] UNKNOWN ASSETTYPE: {0}", assetType);
                        break;
                }
            }
        }
    }
}
