﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class ShopGameplayItemsMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            MainWindow.shopItemsLastLoad = stream.ReadInt();
            int itemsCount = stream.ReadInt();

            for (int i = 0; i < itemsCount; i++)
            {
                int GUID = stream.ReadInt();
                //Log("Shop Item GUID: {0}", GUID);

                int defaultPrice = stream.ReadInt();

                if (GUID == 5301041)
                {
                    defaultPrice = 10;
                }

                int buildPrice = stream.ReadInt();
                int upgradePrice = stream.ReadInt();
                int refillPrice = stream.ReadInt();
                int linkedGUID = stream.ReadInt();
            }
        }
    }
}
