﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class RedirectMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            string redirectUrl = stream.ReadString();
            ushort redirectPort = stream.ReadUnsignedShort();
        }
    }
}
