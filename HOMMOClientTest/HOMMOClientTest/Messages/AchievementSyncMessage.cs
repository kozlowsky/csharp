﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class AchievementSyncMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            string achievementSyncHeroName = stream.ReadString();
            ushort achievementsCount = stream.ReadUnsignedShort();

            for (int i = 0; i < achievementsCount; i++)
            {
                string achievementHeroNameUpdate = stream.ReadString();
                int achievementIDUpdate = stream.ReadInt();

                int achievementValueUpdate = stream.ReadByte();
            }
        }
    }
}
