﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class HeroAchievementPointsMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            string achievementPointsHeroName = stream.ReadString();
            int achievementPoints = stream.ReadInt();
        }
    }
}
