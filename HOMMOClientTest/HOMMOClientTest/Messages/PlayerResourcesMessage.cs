﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class PlayerResourcesMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            int resourcesCount = stream.ReadByte();

            for (int i = 0; i < resourcesCount; i++)
            {
                int val1 = stream.ReadByte();
                int val2 = stream.ReadInt();
                int val3 = stream.ReadInt();
            }
        }
    }
}
