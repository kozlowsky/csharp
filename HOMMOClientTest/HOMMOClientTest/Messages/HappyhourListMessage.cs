﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class HappyhourListMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            int playerGroup = stream.ReadInt();
            int adPopupCooldown = stream.ReadInt();
            int happyHours = stream.ReadInt();

            for (int i = 0; i < happyHours; i++)
            {
                int happyHourDisplayed = stream.ReadInt();

                int ID = stream.ReadInt();
                int headlineTextGUID = stream.ReadInt();
                int bodyTextGUID = stream.ReadInt();
                string imageFile = stream.ReadString();
                int iconGUID = stream.ReadInt();
                int linkTextGUID = stream.ReadInt();
                string linkURL = stream.ReadString();
                int linkTargetGUID = stream.ReadInt();
                int lingering = stream.ReadByte();
                int targetUserGroup = stream.ReadInt();
                uint endTime = stream.ReadUnsignedInt();

                int percentage = stream.ReadInt();
            }
        }
    }
}
