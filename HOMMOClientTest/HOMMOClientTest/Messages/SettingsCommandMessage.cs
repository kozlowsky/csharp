﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class SettingsCommandMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            bool audioEnabled = Convert.ToBoolean(stream.ReadByte());
            int musicVolume = stream.ReadByte();
            int soundVolume = stream.ReadByte();
            bool tutorialEnabled = Convert.ToBoolean(stream.ReadByte());
            bool battleAutoInviteEnabled = Convert.ToBoolean(stream.ReadByte());
            bool battleAutoAcceptEnabled = Convert.ToBoolean(stream.ReadByte());
            int loadLastHero = stream.ReadByte();
            int fastBattle = stream.ReadByte();
            int soundSetting = stream.ReadByte();
            int musicSetting = stream.ReadByte();
            bool autoBattleEnabled = Convert.ToBoolean(stream.ReadByte());
            bool showName = Convert.ToBoolean(stream.ReadByte());
            int graphicsLevel = stream.ReadByte();
            int battleSpeedLevel = stream.ReadByte();
            int chatBubbles = stream.ReadByte();
            int manualBattleFacing = stream.ReadByte();
            int battleCamera = stream.ReadByte();
            int irrelevantGraphics = stream.ReadByte();
        }
    }
}
