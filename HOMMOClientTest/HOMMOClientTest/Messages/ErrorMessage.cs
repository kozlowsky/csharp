﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HOMMOClientTest
{
    class ErrorMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            short errorCode = stream.ReadShort();

            Debug.Log(DebugType.ERROR, "[ERR] Error Code: {0}", errorCode);

            switch (errorCode)
            {
                case 1:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: SERVER_BUSY");
                    break;
                case 2:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: IDLE_TIMEOUT");
                    break;
                case 3:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: NO_GAME_SERBERS");
                    break;
                case 4:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_MANAGER_DISABLED");
                    break;
                case 5:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: GAME_SERVER_NOT_RUNNING");
                    break;
                case 6:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INVALID_MSG_QUEUE_ON_AUTH_ACCEPT");
                    break;
                case 7:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: AUTHENTICATION_QUERY_FAILED");
                    break;
                case 8:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: ALREADY_CONNECTED");
                    break;
                case 9:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: HEADER_SEND_FAILED");
                    break;
                case 10:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: DATA_SEND_FAILED");
                    break;
                case 11:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: ALREADY_ASSIGNED_OR_QUEUED");
                    break;
                case 12:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INVALID_INSTANCE_ID");
                    break;
                case 13:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: SERVER_SHUTDOWN");
                    break;
                case 14:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_CRASH");
                    break;
                case 15:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_TERMINATED");
                    break;
                case 16:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_CLOSED");
                    break;
                case 17:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_SHUTDOWN");
                    break;
                case 18:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_CANCELED");
                    break;
                case 19:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INSTANCE_ASSIGNMENT_FAILED");
                    break;
                case 20:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: PLAYER_CREATION_FAILED");
                    break;
                case 21:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: PLAYER_KICK");
                    break;
                case 22:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: SESSION_HIJACK");
                    break;
                case 23:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: NO_PIPELINING_SUPPORT");
                    break;
                case 24:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: LOADING_HERO_DATA_FAILED");
                    break;
                case 25:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: CLIENT_REGISTRATION_FAILED");
                    break;
                case 26:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INVALID_CLIENT_MESSAGE");
                    break;
                case 27:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: MESSAGE_PROCESSING_ERROR");
                    break;
                case 28:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INVALID_PING");
                    break;
                case 29:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: DATABASE_ERROR");
                    break;
                case 30:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: SESSION_TOKENIZE_FAILED");
                    Debug.Log(DebugType.INFO, "Retrying with different session token...");

                    throw new InvalidSessionTokenException();
                case 31:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: INVALID_SESSION_TOKEN");
                    Debug.Log(DebugType.INFO, "Retrying with different session token...");

                    throw new InvalidSessionTokenException();
                case 32:
                    Debug.Log(DebugType.ERROR, "[ERR|INFO] Error Name: CONNECTED_TO_LOGIN_SERVER");
                    break;
                case 33:
                    Debug.Log(DebugType.ERROR, "[ERR] Error Name: COUNT");
                    break;
                default:
                    Debug.Log(DebugType.WARNING, "[WARN] Error code not handled! [CODE:{0}]", errorCode);
                    break;
            }
        }
    }
}
