﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class AccountCommandMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            bool renameRequest = Convert.ToBoolean(stream.ReadByte());
            int cardsBought = stream.ReadByte();
            int availableHeroes = stream.ReadByte();

            List<Hero> heroes = new List<Hero>();

            for (int i = 0; i < availableHeroes; i++)
            {
                Hero hero = new Hero(stream.ReadInt(), stream.ReadString(), stream.ReadInt(), stream.ReadInt(), stream.ReadInt(), stream.ReadInt(), stream.ReadInt(), stream.ReadInt(), stream.ReadInt(), stream.ReadString());
                heroes.Add(hero);
            }

            //heroesLabel.Text = String.Format("Heroes: {0} ({1}, {2})", availableHeroes, heroes.First().Name, heroes.Last().Name);
            Debug.Log(DebugType.INFO, String.Format("Heroes: {0} ({1}, {2})", availableHeroes, heroes.First().Name, heroes.Last().Name));

            MainWindow.heroes = heroes;
        }
    }
}
