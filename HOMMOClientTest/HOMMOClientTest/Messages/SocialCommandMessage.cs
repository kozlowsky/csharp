﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class SocialCommandMessage : IMessage
    {
        public void processStream(MemoryStream stream)
        {
            int messageCategory = stream.ReadByte();

            switch (messageCategory)
            {
                case 0: // SOCIAL_INVALID
                    break;
                case 2: // SOCIAL_SET
                    string roomId = stream.ReadString();

                    break;
                case 3: // SOCIAL_BROADCAST
                    string systemMessage = stream.ReadString();

                    break;
                case 11: // SOCIAL_INIT
                    string server = stream.ReadString();
                    string host = stream.ReadString();
                    int port = stream.ReadInt();
                    string pass = stream.ReadString();

                    break;
                case 19: // SOCIAL_MESSAGE
                    int messageId = stream.ReadInt();

                    /*if(messageId == [??])
                    {

                    }*/

                    switch (messageId)
                    {
                        case 1201478: // TMFRIEND_LIST_LIMIT_REACHED
                            break;
                            /*case [??]:
                                break;
                            case [??]:
                                break;*/
                    }

                    break;
                case 20: // SOCIAL_CHAT_PRESENCE
                    string targetHeroName = stream.ReadString().ToLower();
                    int isOffline = stream.ReadByte();
                    break;
                default: // Invalid command
                    break;
            }
        }
    }
}
