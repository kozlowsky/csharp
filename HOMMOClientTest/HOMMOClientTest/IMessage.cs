﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    interface IMessage
    {
        void processStream(MemoryStream stream);
    }
}
