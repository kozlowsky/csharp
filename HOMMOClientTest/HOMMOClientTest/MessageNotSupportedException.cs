﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class MessageNotSupportedException : Exception
    {
        public MessageNotSupportedException()
        {

        }

        public MessageNotSupportedException(string message) : base(message)
        {

        }
    }
}
