﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HOMMOClientTest
{
    class Networking
    {
        private string username = "";
        private string password = "";

        public Stream SendStream(string url, int sessionToken, String sessionId, List<byte> bufferData)
        {
            if (MainWindow.abortExecution)
                return null;

            List<byte> sendBuffer = new List<byte>();
            sendBuffer.WriteInt(sessionToken);
            sendBuffer.WriteString(sessionId);

            sendBuffer.AddRange(bufferData);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/octet-stream";
            httpWebRequest.KeepAlive = true;
            httpWebRequest.ContentLength = sendBuffer.Count;
            httpWebRequest.Timeout = 5000;

            Stream receiveStream = null;
            HttpWebResponse httpWebResponse = null;

            try
            {
                using (Stream dataStream = httpWebRequest.GetRequestStream())
                {
                    dataStream.Write(sendBuffer.ToArray(), 0, sendBuffer.Count);
                }

                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                receiveStream = httpWebResponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                Debug.Log(DebugType.ERROR, "ERROR: {0}", ex.Message);
            }

            return receiveStream;
        }

        public void LoginUsingHttp(ref string sessionId)
        {
            Debug.Log(DebugType.INFO, "Getting session token from website...");

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;

            FormUrlEncodedContent postContainer = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("name", username),
                new KeyValuePair<string, string>("password", password)
                });

            HttpClient client = new HttpClient(handler);

            HttpResponseMessage response = client.PostAsync(UrlData.WebsiteApiUrl, postContainer).Result;
            response = client.GetAsync(UrlData.WebsiteUrl).Result;

            try
            {
                sessionId = cookies.GetCookies(new Uri(UrlData.WebsiteUrl))["dsoAuthToken"].Value;
                Debug.Log(DebugType.INFO, "[CLIENT] Received new session hash: {0}", sessionId);
            }
            catch (Exception ex)
            {
                Debug.Log(DebugType.ERROR, "Could not login to the website! Error: {0}", ex.Message);
            }
        }
    }
}
