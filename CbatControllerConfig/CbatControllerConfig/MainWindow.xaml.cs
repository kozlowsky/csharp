﻿using System.Windows;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;
using System.Diagnostics;
using System;
using System.Windows.Controls;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace CbatControllerConfig
{
    public partial class MainWindow : Window
    {
        private string romDirPath = "";
        private string mtFilePath = "";
        private string configFilePath = "config.txt";
        private string oldRomPath = "";

        public ObservableCollection<Controller> conts;

        public MainWindow()
        {
            InitializeComponent();

            conts = new ObservableCollection<Controller>();

            oldRomPath = Properties.Settings.Default.oldRomPath;
            romSelectInput.Text = oldRomPath;
            romDirPath = oldRomPath;

            biosVerInput.Text = Properties.Settings.Default.biosVersion;

            LoadConfigIntoList("config.txt").GetAwaiter().OnCompleted(() => { Console.WriteLine("Loaded config list"); });
        }

        public class Controller : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            private string contIp;
            private string platformType;
            private string testgroupName;
            private string platformBoardType;
            private bool enabled;
            private SolidColorBrush resultColor;
            private ICommand showLogCommand;
            private bool _canExecute;

            public string ContIp
            {
                get { return contIp; }
                set { contIp = value; OnPropertyChanged("ContIp"); }
            }

            public string PlatformType
            {
                get { return platformType; }
                set { platformType = value; OnPropertyChanged("PlatformType"); }
            }

            public string TestgroupName
            {
                get { return testgroupName; }
                set { testgroupName = value; OnPropertyChanged("TestgroupName"); }
            }

            public string PlatformBoardType
            {
                get { return platformBoardType; }
                set { platformBoardType = value; OnPropertyChanged("PlatformBoardType"); }
            }

            public bool Enabled
            {
                get { return enabled; }
                set { enabled = value; OnPropertyChanged("Enabled"); }
            }

            public SolidColorBrush ResultColor
            {
                get { return resultColor; }
                set { resultColor = value; OnPropertyChanged("ResultColor"); }
            }

            public string Log { get; set; }

            public ICommand ShowLogCommand
            {
                get
                {
                    return showLogCommand ?? (showLogCommand = new CommandHandler(() => MessageBox.Show(Log), _canExecute));
                }
            }

            public Controller(string contIp, string platformType, string testgroupName, string platformBoardType, bool enabled)
            {
                _canExecute = true;

                resultColor = SystemColors.ControlBrush;

                this.contIp = contIp;
                this.platformType = platformType;
                this.testgroupName = testgroupName;
                this.platformBoardType = platformBoardType;
                this.enabled = enabled;
            }

            protected void OnPropertyChanged(string name)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }

        public class CommandHandler : ICommand
        {
            private Action _action;
            private bool _canExecute;
            public CommandHandler(Action action, bool canExecute)
            {
                _action = action;
                _canExecute = canExecute;
            }

            public bool CanExecute(object parameter)
            {
                return _canExecute;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                _action();
            }
        }

        public class Colors
        {
            public static SolidColorBrush Green = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#00ff00"));
            public static SolidColorBrush Red = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#ff0000"));
            public static SolidColorBrush Yellow = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#ffff00"));
        }

        private async Task LoadConfigIntoList(string configFilePath)
        {
            conts.Clear();

            StreamReader configFile = new StreamReader(configFilePath);

            string line = "";

            while ((line = await configFile.ReadLineAsync()) != null)
            {
                bool enabled = true;

                if (line.StartsWith("//"))
                {
                    line = line.Substring(2);
                    enabled = false;
                }

                string[] lineParams = line.Split(';');

                Controller cont = new Controller(lineParams[0], lineParams[1], lineParams[2], lineParams[3], enabled);
                conts.Add(cont);
            }

            contListBox.ItemsSource = conts;
        }

        private void romSelectButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            CommonFileDialogResult result = dialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                romDirPath = dialog.FileName;
                romSelectInput.Text = romDirPath;

                Properties.Settings.Default.oldRomPath = romDirPath;
                Properties.Settings.Default.Save();
            }
        }

        private void mtFileButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            CommonFileDialogResult result = dialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                mtFilePath = dialog.FileName;
                mtFileInput.Text = mtFilePath;
            }
        }

        private void configButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            CommonFileDialogResult result = dialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                configFilePath = dialog.FileName;
                configInput.Text = configFilePath;

                LoadConfigIntoList(configFilePath).GetAwaiter().OnCompleted(() => { });
            }
        }

        private async void runButton_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.biosVersion = biosVerInput.Text;
            Properties.Settings.Default.Save();

            ((Button)sender).Content = "Running...";
            ((Button)sender).IsEnabled = false;

            await Run();

            ((Button)sender).Content = "RUN";
            ((Button)sender).IsEnabled = true;
        }

        private async Task Run()
        {
            foreach (var cont in conts)
            {
                bool value = false;

                if (cont.Enabled)
                {
                    cont.ResultColor = Colors.Yellow;
                    value = await ProcessController(cont);
                }
                else
                    continue;

                if (value)
                {
                    cont.ResultColor = Colors.Green;
                }
                else
                {
                    cont.ResultColor = Colors.Red;
                }
            }
        }

        private async Task<bool> ProcessController(Controller cont)
        {
            string biosArg = "bios=";
            if (dailyCheckbox.IsChecked.Value)
                biosArg += "daily";
            else
                biosArg += "label";

            string groupnameExt = "";
            if (smallCheckbox.IsChecked != null)
                groupnameExt = "_Small";

            string contIp = cont.ContIp;
            string platformTypeArg = "type=" + cont.PlatformType;
            string testGroupName = "testname=" + cont.TestgroupName;
            string platformBoardArg = "board=" + cont.PlatformBoardType;

            string args = "-u IntelAdmin -p 11qq!!QQ \\\\" + contIp + " -c -f \"CbatRomChanger.exe\" rom=" + romDirPath + " " + platformTypeArg + " " + testGroupName + groupnameExt + " mt=" + mtFilePath + " " + biosArg + " biosver=" + biosVerInput.Text + " " + platformBoardArg;

            using (var process = new Process
            {
                StartInfo =
                    {
                        FileName = "psexec.exe", Arguments = args,
                        UseShellExecute = false, CreateNoWindow = true,
                        RedirectStandardOutput = true, RedirectStandardError = false
                    },
                EnableRaisingEvents = true
            })
            {
                await RunProcessAsync(process, cont).ConfigureAwait(false);
            }

            //cont.Log = cont.Log.Replace("PsExec v2.2 - Execute processes remotely\n", "").Replace("Copyright (C) 2001-2016 Mark Russinovich\n", "").Replace("Sysinternals - www.sysinternals.com\n", "");

            if (cont.Log.Contains("Success"))
                return true;

            return false;
        }

        private Task<int> RunProcessAsync(Process process, Controller cont)
        {
            var tcs = new TaskCompletionSource<int>();

            process.Exited += (s, ea) => tcs.SetResult(process.ExitCode);
            process.OutputDataReceived += (s, ea) => cont.Log += ea.Data + "\n";
            //process.ErrorDataReceived += (s, ea) => cont.Log += ea.Data + "\n";

            bool started = process.Start();
            if (!started)
            {
                throw new InvalidOperationException("Could not start process: " + process);
            }

            process.BeginOutputReadLine();
            //process.BeginErrorReadLine();

            return tcs.Task;
        }

        private void dailyCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (labelCheckbox != null)
                labelCheckbox.IsChecked = false;
        }

        private void labelCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (dailyCheckbox != null)
                dailyCheckbox.IsChecked = false;
        }
    }
}
