﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;

namespace CbatRomChanger
{
    class Program
    {
        private static string romDirPath = "";
        private static string mtFilePath = "";
        private static string platformType = "";
        private static string testGroupName = "";
        private static string biosType = "";
        private static string biosVersion = "";
        private static string platformBoard = "";

        private static string romPath = "";
        private static string romName = "";
        private static string romNameNoExt = "";
        private static string debugRomPath = "";
        private static string debugRomName = "";
        private static string newRomPath = "";
        private static string newDebugRomPath = "";

        private static string complexDirPath = "";
        private static string complexBinsPath = "";

        private static StringComparison comp;

        static void Main(string[] args)
        {
            if (args.Length == 0)
                return;

            foreach(string arg in args)
            {
                if(arg.Contains("rom=", comp) )
                {
                    romDirPath = arg.Split('=')[1];
                }

                if(arg.Contains("mt=", comp))
                {
                    mtFilePath = arg.Split('=')[1];
                }

                if(arg.Contains("type=", comp))
                {
                    platformType = arg.Split('=')[1];
                }

                if (arg.Contains("testname=", comp))
                {
                    testGroupName = arg.Split('=')[1];
                }

                if (arg.Contains("bios=", comp))
                {
                    biosType = arg.Split('=')[1];
                }

                if (arg.Contains("biosver=", comp))
                {
                    biosVersion = arg.Split('=')[1];
                }

                if (arg.Contains("board=", comp))
                {
                    platformBoard = arg.Split('=')[1];
                }
            }

            complexDirPath = GetDesktopDir() + "\\COMPLEX\\";
            complexBinsPath = complexDirPath + "\\BINS\\";

            comp = StringComparison.OrdinalIgnoreCase;

            try
            {
                CopyRom();
                ChangeConfig();
                BackupHistory();
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            
            Console.WriteLine("Success");
        }

        private static string GetDesktopDir()
        {
            string adminDir = @"C:\Users\Administrator\Desktop";
            string altAdminDir = @"C:\Users\Administrator\Desktop";

            if (Directory.Exists(adminDir))
                return adminDir;
            else
                return altAdminDir;
        }

        private static void BackupHistory()
        {
            try
            {
                MoveDirectory(@"C:\History", @"C:\History.old");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: backup History");
                throw;
            }
        }

        private static void InstallMT()
        {

        }

        private static void CopyRom()
        {
            DirectoryInfo directory = null;

            NetworkCredential credentials = new NetworkCredential("PchBiosLab", "!&Q$Pch");

            using (new NetworkConnection(@"\\10.102.27.70", credentials))
            {
                try
                {
                    directory = new DirectoryInfo(@romDirPath);

                    foreach (FileInfo file in directory.GetFiles())
                    {
                        if (file.Name.Contains(platformType, comp))
                        {
                            if (!file.Name.Contains("debug", comp))
                            {
                                romPath = file.FullName;
                                romName = file.Name;
                            }
                            else
                            {
                                debugRomPath = file.FullName;
                                debugRomName = file.Name;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Can't find NAS directory or files!");
                    Console.WriteLine(ex.Message);
                    throw;
                }

                romNameNoExt = romName.Replace(".rom", "");

                if (!File.Exists(romPath))
                {
                    Console.WriteLine("Rom file not found!");
                    return;
                }

                newRomPath = complexBinsPath + romName;
                newDebugRomPath = complexBinsPath + debugRomName;

                try
                {
                    if (!File.Exists(newRomPath))
                        File.Copy(romPath, newRomPath, false);

                    if (!File.Exists(newDebugRomPath))
                        File.Copy(debugRomPath, newDebugRomPath, false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error copying files! " + ex.Message);
                    throw;
                }

                if (!File.Exists(newRomPath))
                    Console.WriteLine("Release rom not copied!");

                if (!File.Exists(newDebugRomPath))
                    Console.WriteLine("Debug rom not copied!");
            }
        }

        private static void ChangeConfig()
        {
            XmlDocument config = new XmlDocument();
            XmlDocument debugConfig = new XmlDocument();
            config.PreserveWhitespace = true;
            debugConfig.PreserveWhitespace = true;

            try { config.Load(complexDirPath + "latest/configs/common.xml"); }
            catch (FileNotFoundException) { Console.WriteLine("No config!"); throw; }

            try { debugConfig.Load(complexDirPath + "latest/configs/debug.xml"); }
            catch (FileNotFoundException) { Console.WriteLine("No debug config!"); throw; }

            config.SelectSingleNode("ArrayOfAnyType/anyType/ImagePreparation/BiosRomImagePath").InnerXml = newRomPath;
            config.SelectSingleNode("ArrayOfAnyType/anyType/ScenarioSetup/TestGroupName").InnerXml = romNameNoExt + testGroupName;

            if(biosType != "")
            {
                string resultsPath = @"\\10.102.27.70\Results\CNL\" + biosType + "\\" + biosVersion + "\\FSP\\" + platformBoard;

                try
                {
                    config.SelectSingleNode("ArrayOfAnyType/anyType/ScenarioSetup/CBat100ResultsSharePath").InnerXml = resultsPath;
                }
                catch (Exception ex)
                {
                    XmlElement cbatElement = config.CreateElement(string.Empty, "CBat100ResultsSharePath", string.Empty);
                    XmlText cbatContent = config.CreateTextNode(resultsPath);

                    cbatElement.AppendChild(cbatContent);

                    XmlNode xml = config.SelectSingleNode("ArrayOfAnyType/anyType/ScenarioSetup");
                    xml.AppendChild(cbatElement);
                }
            }
            
            debugConfig.SelectSingleNode("ArrayOfAnyType/anyType/ImagePreparation/BiosRomImagePath").InnerXml = newDebugRomPath;

            config.Save(complexDirPath + "latest/configs/common.xml");
            debugConfig.Save(complexDirPath + "latest/configs/debug.xml");
        }

        public static void MoveDirectory(string source, string target)
        {
            var sourcePath = source.TrimEnd('\\', ' ');
            var targetPath = target.TrimEnd('\\', ' ');

            if (!Directory.Exists(sourcePath))
                return;

            var files = Directory.EnumerateFiles(sourcePath, "*", SearchOption.AllDirectories)
                                 .GroupBy(s => Path.GetDirectoryName(s));
            foreach (var folder in files)
            {
                var targetFolder = folder.Key.Replace(sourcePath, targetPath);
                Directory.CreateDirectory(targetFolder);
                foreach (var file in folder)
                {
                    var targetFile = Path.Combine(targetFolder, Path.GetFileName(file));
                    if (File.Exists(targetFile)) File.Delete(targetFile);
                    File.Move(file, targetFile);
                }
            }
            Directory.Delete(source, true);
        }
    }

    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}
